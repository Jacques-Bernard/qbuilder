# -*- coding: utf-8 -*-
"""

"""
from PyQt5 import QtWidgets, QtCore, QtGui

clavier = dict()
clavier['CTRL+A'] = QtCore.Qt.CTRL + QtCore.Qt.Key_A
clavier['CTRL+D'] = QtCore.Qt.CTRL + QtCore.Qt.Key_D
clavier['CTRL+E'] = QtCore.Qt.CTRL + QtCore.Qt.Key_E
clavier['CTRL+O'] = QtCore.Qt.CTRL + QtCore.Qt.Key_O
clavier['CTRL+Q'] = QtCore.Qt.CTRL + QtCore.Qt.Key_Q
clavier['CTRL+S'] = QtCore.Qt.CTRL + QtCore.Qt.Key_S


class QBuilder(QtWidgets.QMainWindow):
    BOX_LAYOUT = 'Box_Layout'
    HORIZONTAL = 'H'
    VERTICAL = 'V'
    GRID = 'G'
    LABEL = 'Label'
    COMBOBOX = 'Combobox'
    COMBOBOX_RADIO = 'Combobox_Radio'
    COMBOBOX_CHECK = 'Combobox_Check'
    BUTTON = 'Button'
    LINE_EDIT = 'Line_Edit'
    FIGURE = 'Figure'
    TEXT_EDIT = 'Text_Edit'
    IMAGE = 'Image'

    change_widget_language = []

    def noisy(self, msg: str):
        if self._verbose:
            print(msg)

    def box_layout(self, parent: QtWidgets.QWidget, conf: dict):
        """
        BoxLayout
        :param parent: parent object
        :param conf: configuration
            mandatory : 'Type': QBuilder.BOX_LAYOUT
            option :
                'Orientation': QBuilder.HORIZONTAL | QBuilder.VERTICAL
                'Contenu' : [ ... ] | ...
        :return:
        """
        if conf['Type'] != QBuilder.BOX_LAYOUT:
            return False
        self.noisy('Type ' + QBuilder.BOX_LAYOUT + ':')
        orientation = QBuilder.VERTICAL
        try:
            orientation = conf['Orientation']
        except KeyError:
            pass
        if orientation == QBuilder.HORIZONTAL:
            self.noisy('  Orientation: ' + QBuilder.HORIZONTAL)
            if isinstance(parent, QtWidgets.QWidget):
                # Case : parent is widget
                q_layout: QtWidgets.QBoxLayout = QtWidgets.QHBoxLayout(parent)
            else:
                # Case : parent is layout
                q_layout: QtWidgets.QBoxLayout = QtWidgets.QHBoxLayout()
        elif orientation == QBuilder.VERTICAL:
            self.noisy('  Orientation: ' + QBuilder.VERTICAL)
            if isinstance(parent, QtWidgets.QWidget):
                # Case : parent is widget
                q_layout: QtWidgets.QBoxLayout = QtWidgets.QVBoxLayout(parent)
            else:
                # Case : parent is layout
                q_layout: QtWidgets.QBoxLayout = QtWidgets.QVBoxLayout()
        else:
            self.noisy('  Orientation: ##Error (H ou V)')
            return False
        if isinstance(parent, QtWidgets.QLayout):
            parent.addLayout(q_layout)
        try:
            for conf in conf['Contenu']:
                self.interprete(q_layout, conf)
        except KeyError:
            try:
                self.interprete(q_layout, conf['Contenu'])
            except KeyError:
                pass
        return True

    def trad(self, dico):
        """
        Si dico est une chaîne de caractères, on l'a retourne (pas de
        declinaison linguistique).
        Si dico est un dictionnaire dont la clef est une chaine decrivant une
        langue comme 'fr', 'en'... On retourne la declinaison pour la langue
        demandée : self._lang.
        :param dico:
        :return:
        """
        if isinstance(dico, dict):
            try:
                return dico[self._lang]
            except KeyError:
                return ('No translation available')
        try:
            return dico(self._lang)
        except KeyError:
            return dico

    def label(self, parent, conf):
        """
        Les clefs du dictionnaire:
        - obligatoire
            'Type': QBuilder.LABEL,
        - optionnelles :
            'Text': '...' | { 'fr': '...', 'en':..., }
            'Font': { 'Size' : #
                      'Bold' : True | False
            'Link': ( 'Text' | 'Enabled', fct )
        )
        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.LABEL:
            return False
        self.noisy(QBuilder.LABEL + ':')
        #
        try:
            q_label: QtWidgets.QLabel = QtWidgets.QLabel(self.trad(conf['Text']))
            self.noisy('   Text: ' + self.trad(conf['Text']))
            self.change_widget_language.append(
                lambda self=self, q_label=q_label, text=conf['Text']:
                q_label.setText(self.trad(text))
            )
        except KeyError:
            self.noisy('  not attribute Text')
        font = q_label.font()
        try:
            font.setPointSize(conf['Font']['Size'])
        except KeyError:
            pass
        try:
            font.setBold(conf['Font']['Bold'])
        except KeyError:
            pass
        try:
            q_label.setToolTip(self.trad(conf['Bulle']))
            self.noisy('   Bulle: ' + self.trad(conf['Bulle']))
            self.change_widget_language.append(
                lambda self=self, q_label=q_label, text=conf['Bulle']:
                q_label.setToolTip(self.trad(text))
            )
        except KeyError:
            pass
        try:
            if conf['Link'][1] == 'Text':
                conf['Link'][0](conf['Link'][2], qlabel.setText)
            elif conf['Link'][1] == 'Enabled':
                conf['Link'][0](conf['Link'][2], qlabel.setEnabled)
        except KeyError:
            pass
        q_label.setFont(font);
        parent.addWidget(q_label)
        return True

    def line_edit(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.LINE_EDIT:
            return False
        self.noisy(QBuilder.LINE_EDIT + ':')
        #
        q_line_edit: QtWidgets.QLineEdit = QtWidgets.QLineEdit()
        try:
            q_line_edit.setText(self.trad(conf['Text']))
            self.noisy('   Text: ' + self.trad(conf['Text']))
        except KeyError:
            self.noisy('  not attribute Text')
        font = q_line_edit.font();
        try:
            font.setPointSize(conf['Font']['Size'])
        except KeyError:
            pass
        try:
            font.setBold(conf['Font']['Bold'])
        except KeyError:
            pass
        q_line_edit.setFont(font);
        try:
            q_line_edit.editingFinished.connect(lambda: conf['Action'](q_line_edit.displayText()))
        except KeyError:
            pass
        try:
            q_line_edit.setToolTip(self.trad(conf['Bulle']))
            self.noisy('   Bulle: ' + self.trad(conf['Bulle']))
            self.change_widget_language.append(
                lambda self=self, q_line_edit=q_line_edit, text=conf['Bulle']:
                q_line_edit.setToolTip(self.trad(text))
            )
        except KeyError:
            pass
        try:
            if conf['Link'][1] == 'Text':
                conf['Link'][0](conf['Link'][2], q_line_edit.setText)
            elif conf['Link'][1] == 'Enabled':
                conf['Link'][0](conf['Link'][2], q_line_edit.setEnabled)
        except KeyError:
            pass
        parent.addWidget(q_line_edit)
        return True

    def button(self, parent, conf):
        """"

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.BUTTON:
            return False
        self.noisy(QBuilder.BUTTON + ':')
        #
        q_button = None
        try:
            q_button = QtWidgets.QPushButton(self.trad(conf['Text']))
            self.noisy('   Text: ' + self.trad(conf['Text']))
            self.change_widget_language.append(
                lambda self=self, q_button=q_button, text=conf['Text']:
                q_button.setText(self.trad(text))
            )
        except KeyError:
            self.noisy('  not attribute Text')
        try:
            q_button.clicked.connect(conf['Action'])
        except KeyError:
            print('manque Action')
        try:
            q_button.setEnabled(conf['Enabled'])
        except KeyError:
            pass
        try:
            q_button.setStyleSheet(conf['StyleSheet'])
        except KeyError:
            pass
        parent.addWidget(q_button)
        try:
            save = conf['Save']
            save[0](save[1], q_button)
        except KeyError:
            pass
        try:
            q_button.setToolTip(conf['Bulle'][self._lang])
            self.change_widget_language.append(
                lambda self=self, q_button=q_button, text=conf['Bulle']:
                q_button.setToolTip(self.trad(text))
            )
        except KeyError:
            pass
        try:
            links = [conf['Link']]
            if isinstance(conf['Link'], list):
                links = conf['Link']
            for link in links:
                if link[1] == 'Text':
                    link[0](link[2], q_button.setText)
                elif link[1] == 'Enabled':
                    link[0](link[2], q_button.setEnabled)
                elif link[1] == 'StyleSheet':
                    link[0](link[2], q_button.setStyleSheet)
        except KeyError:
            pass
        return True

    def change_lang_group_box(self, groupbox, listes):
        liste = self.trad(listes)
        groupbox.setTitle(liste)

    def avant_group(self, conf):
        try:
            group = QtWidgets.QGroupBox(self.trad(conf['Title']))
            self.noisy('   Title: ' + self.trad(conf['Title']))
            group.setFlat(False)
            self.change_widget_language.append(
                lambda self=self, qcombobox=group, listes=conf['Title']:
                self.change_lang_group_box(group, listes)
            )
            return group
        except KeyError:
            return None

    def apres_group(self, conf, parent, group, layout, widget):
        if group:
            try:
                group.setToolTip(conf['Bulle'][self._lang])
                self.change_widget_language.append(
                    lambda self=self, group=group, text=conf['Bulle']:
                    group.setToolTip(self.trad(text))
                )
            except KeyError:
                pass
            group.setLayout(layout)
            parent.addWidget(group)
        else:
            try:
                widget.setToolTip(conf['Bulle'][self._lang])
                self.change_widget_language.append(
                    lambda self=self, widget=widget, text=conf['Bulle']:
                    widget.setToolTip(self.trad(text))
                )
            except KeyError:
                pass
            parent.addWidget(widget)

    def change_lang_combobox(self, combobox, listes):
        liste = self.trad(listes)
        for i, value in enumerate(liste):
            combobox.setItemText(i, value)

    def combobox(self, parent, conf):
        """
        ('ComboBox',
         {'Title':
          'Bulle':
          'Choices':
          'Check':
          'Action':
          'Save':
         }
         [...]
        )
        """
        if conf['Type'] != QBuilder.COMBOBOX:
            return False
        self.noisy(QBuilder.COMBOBOX + ':')
        #
        group = self.avant_group(conf)
        qcombobox = QtWidgets.QComboBox()
        choices = self.trad(conf['Choices'])
        qcombobox.addItems(choices)
        self.change_widget_language.append(
            lambda self=self, qcombobox=qcombobox, listes=conf['Choices']:
            self.change_lang_combobox(qcombobox, listes)
        )
        #
        qcombobox.currentIndexChanged.connect(lambda x: conf['Action'](x, choices[x]))
        if isinstance(conf['Check'], int):
            qcombobox.setCurrentIndex(conf['Check'])
        else:
            qcombobox.setCurrentIndex(qcombobox.findText(self.trad(conf['Check'])))
        #
        try:
            save = conf['Save']
            save[0](save[1], qcombobox)
        except KeyError:
            pass
        #
        try:
            if conf['Orientation'] == 'V':
                layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
            else:
                layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        except KeyError:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        layout.addWidget(qcombobox)
        layout.addStretch(1)
        self.apres_group(conf, parent, group, layout, qcombobox)
        #
        try:
            if conf['Link'][1] == 'Text':
                conf['Link'][0](conf['Link'][2],
                                lambda text, qcombobox=qcombobox: qcombobox.setCurrentIndex(qcombobox.findText(text)))
            if conf['Link'][1] == '#Text':
                conf['Link'][0](conf['Link'][2], qcombobox.setCurrentIndex)
            elif conf['Link'][1] == 'Enabled':
                conf['Link'][0](conf['Link'][2], qcombobox.setEnabled)
        except KeyError:
            pass
        #
        return True

    def combobox_radio(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.COMBOBOX_RADIO:
            return False
        self.noisy(QBuilder.COMBOBOX_RADIO + ':')
        #
        group = self.avant_group(conf)
        checkboxes = []
        try:
            for i, choice in enumerate(conf['Choices'][self._lang]):
                checkbox = QtWidgets.QRadioButton(choice)
                print(conf['Check'])
                if isinstance(conf['Check'], int):
                    check = conf['Choices'][self._lang][conf['Check']]
                else:
                    check = self.trad(conf['Check'])
                if check == choice:
                    checkbox.setChecked(True)
                else:
                    checkbox.setChecked(False)
                checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                checkboxes.append(checkbox)
        except KeyError:
            for i, choice in enumerate(conf['Choices']):
                checkbox = QtWidgets.QRadioButton(choice)
                print(conf['Check'])
                if isinstance(conf['Check'], int):
                    check = conf['Choices'][self._lang][conf['Check']]
                else:
                    check = self.trad(conf['Check'])
                if check == choice:
                    checkbox.setChecked(True)
                else:
                    checkbox.setChecked(False)
                checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                checkboxes.append(checkbox)
        try:
            if conf['Orientation'] == 'V':
                layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
            else:
                layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        except:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        #
        bulle = None
        try:
            bulle = conf['Bulle'][self._lang]
        except KeyError:
            pass
        for checkbox in checkboxes:
            layout.addWidget(checkbox)
            if bulle:
                checkbox.setToolTip(bulle)
                self.change_widget_language.append(
                    lambda self=self, checkbox=checkbox, text=conf['Bulle']:
                    checkbox.setToolTip(self.trad(text))
                )
        layout.addStretch(1)
        if group:
            group.setLayout(layout)
            parent.addWidget(group)
        else:
            parent.addLayout(layout)
        return True

    def combobox_check(self, parent, conf):
        """
        fct: le premier parametre est un booleen qui donne le type,
            le second donne l'identifiant concerne par rapport a ids
        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.COMBOBOX_CHECK:
            return False
        self.noisy(QBuilder.COMBOBOX_CHECK + ':')
        #
        group = self.avant_group(conf)
        checkboxes = []
        try:
            conf['Choices']
            try:
                for i, (choice, check) in enumerate(zip(self.trad(conf['Choices']), conf['Checkes'])):
                    checkbox = QtWidgets.QCheckBox(choice)
                    checkbox.setChecked(check)
                    checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                    checkboxes.append(checkbox)
            except KeyError:
                for i, choice in enumerate(self.trad(conf['Choices'])):
                    checkbox = QtWidgets.QCheckBox(choice)
                    checkbox.setChecked(conf['Checkes'])
                    checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                    checkboxes.append(checkbox)
        except KeyError:
            try:
                for i, (choice, check) in enumerate(zip(self.trad(conf['Choices']), conf['Checkes'])):
                    checkbox = QtWidgets.QCheckBox(choice)
                    checkbox.setChecked(check)
                    checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                    checkboxes.append(checkbox)
            except KeyError:
                for i, choice in enumerate(self.trad(conf['Choices'])):
                    checkbox = QtWidgets.QCheckBox(choice)
                    checkbox.setChecked(conf['Checkes'])
                    checkbox.toggled.connect(lambda x, y=i, z=choice: conf['Action'](x, y, z))
                    checkboxes.append(checkbox)
        nbColumns = -1
        iColumn = 0
        iLine = 0
        try:
            if conf['Orientation'] == QBuilder.GRID:
                layout: QtWidgets.QLayout = QtWidgets.QGridLayout()
                nbColumns = conf['GridColumns']
            elif conf['Orientation'] == QBuilder.VERTICAL:
                layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
            else:
                # Valeur par defaut
                # else conf['Orientation'] == QBuilder.HORIZONTAL:
                layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        except KeyError:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
            #
        bulle = None
        try:
            bulle = conf['Bulle'][self._lang]
        except KeyError:
            pass
        for checkbox in checkboxes:
            if nbColumns != -1:
                layout.addWidget(checkbox, iLine, iColumn)
                iColumn += 1
                if iColumn == nbColumns:

                    iColumn = 0
                    iLine += 1
            else:
                layout.addWidget(checkbox)
            if bulle:
                checkbox.setToolTip(bulle)
                self.change_widget_language.append(
                    lambda self=self, checkbox=checkbox, text=conf['Bulle']:
                    checkbox.setToolTip(self.trad(text))
                )
        if nbColumns == -1:
            layout.addStretch(1)
        if group:
            group.setLayout(layout)
            parent.addWidget(group)
        else:
            parent.addLayout(layout)
        return True

    def text_edit(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.TEXT_EDIT:
            return False
        self.noisy(QBuilder.TEXT_EDIT + ':')
        #
        parent.addWidget(QtWidgets.QTextEdit())
        return True

    def image(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.IMAGE:
            return False
        self.noisy(QBuilder.IMAGE + ':')
        #
        qlabel = QtWidgets.QLabel()
        qpixmap = QtGui.QPixmap(conf['Src'])
        qlabel.setPixmap(qpixmap)
        # Optional, resize window to image size
        try:
            width = conf['Width']
        except KeyError:
            width = None
        try:
            height = conf['Height']
        except KeyError:
            height = None
        if width is None:
            if height is None:
                pass
            else:
                width = (qpixmap.width() * height) / qpixmap.height()
                qlabel.resize(width, height)
        else:
            if height is None:
                height = int((qpixmap.height() * width) / qpixmap.width())
            qlabel.resize(width, height)
        parent.addWidget(qlabel)
        return True

    def figure(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if conf['Type'] != QBuilder.FIGURE:
            return False
        self.noisy(QBuilder.FIGURE + ':')
        #
        parent.addWidget(conf['ClassFigure'](self.qmain))
        return True

    def interprete(self, parent, conf):
        """

        :param parent:
        :param conf:
        :return:
        """
        if self.box_layout(parent, conf):
            return
        if self.label(parent, conf):
            return
        if self.line_edit(parent, conf):
            return
        if self.button(parent, conf):
            return
        if self.combobox(parent, conf):
            return
        if self.combobox_radio(parent, conf):
            return
        if self.combobox_check(parent, conf):
            return
        if self.text_edit(parent, conf):
            return
        if self.image(parent, conf):
            return
        if self.figure(parent, conf):
            return

    def change_language(self, lang):
        self._lang = lang
        for widget in self.change_widget_language:
            widget()
        self.initializeMenuBar(self._conf)

    def initialize_menu_bar(self, conf):
        self._conf: dict = conf
        menuBar = QtWidgets.QMenuBar()
        #
        for menu in conf['MENU']:
            try:
                qmenu = QtWidgets.QMenu(menu[0][self._lang], self)
            except:
                qmenu = QtWidgets.QMenu(menu[0], self)
            for submenu in menu[1]:
                p1 = p2 = p3 = None
                try:
                    p1 = submenu[0][self._lang]
                except:
                    p1 = submenu[0]
                p2 = submenu[1]
                try:
                    p3 = clavier[submenu[2][self._lang]]
                except:
                    try:
                        p3 = clavier[submenu[2]]
                    except:
                        p3 = None
                if p3:
                    qmenu.addAction(p1, p2, p3)
                else:
                    qmenu.addAction(p1, p2)
            menuBar.addMenu(qmenu)
        #
        self.setMenuBar(menuBar)

    def __init__(self, conf):
        self._verbose = False
        self._lang = conf['Langague']
        #
        QtWidgets.QMainWindow.__init__(self)
        # Title
        self.setWindowTitle(conf['TITLE'])
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        # Menu
        self.initialize_menu_bar(conf)
        #
        self.qmain = QtWidgets.QWidget(self)
        self.qmain.setContentsMargins(0, 0, 0, 0)
        # Widgets
        if self._verbose: print('LAYOUT')
        self.interprete(self.qmain, conf['LAYOUT'])
        #
        self.qmain.setFocus()
        self.setCentralWidget(self.qmain)


def qbuilder(console, kernels, reset):
    import sys
    qApp = QtWidgets.QApplication(sys.argv)
    aw = QBuilder(console)
    for kernel in kernels:
        kernel.setApplication(aw)
    reset()
    aw.show()
    sys.exit(qApp.exec_())


def main():
    '''Exemple d'utilisation'''

    class Kernel:
        def run(self):
            pass

    kernel = Kernel()

    class KernelLangague:
        _sigle_langagues = ['fr', 'en', ]  # the first is the default langague
        _dico_langagues = {
            'fr': ['Français', 'Anglais', ],
            'en': ['French', 'English', ],
        }
        _langague_courant = 0

        def updateConfiguration(self, configuration):
            configuration['datation'] = self._dico_datation['fr'][self._datation_courant]

        def __init__(self):
            self._application = None
            self._langague_courant = 0

        def setApplication(self, application):
            '''
            Memorisation de l'application afin de pouvoir appeler changeLangague.
            '''
            self._application = application

        def getAllLangague(self, lang):
            return self._dico_langagues[lang]

        def getPosLangague(self):
            return self._langague_courant

        def getSigleLangague(self):
            return self._sigle_langagues[self._langague_courant]

        def setLangague(self, ind, _):
            self._langague_courant = ind
            self._application.change_langage(self._sigle_langagues[ind])

    kLangague = KernelLangague()

    class KernelMenu:
        _application = None

        def quit(self):
            self._application.close()

        def setApplication(self, application):
            '''
            Memorisation de l'application afin de pouvoir appeler changeLangague.
            '''
            self._application = application

    kMenu = KernelMenu()

    console_menu = [
        ({
             'fr': '&Fichier',
             'en': '&File',
         },
         [({
               'fr': '&Quitter',
               'en': '&Quit',
           },
           kMenu.quit,
           {
               'fr': 'CTRL+Q',
               'en': 'CTRL+Q',
           },
         ),
         ],
        ),
    ]

    console_layout = {
        'Type': QBuilder.BOX_LAYOUT,
        'Orientation': QBuilder.VERTICAL,
        'Contenu': [
            {
                'Type': QBuilder.BOX_LAYOUT,
                'Orientation': QBuilder.HORIZONTAL,
                'Contenu': [
                    {'Type': QBuilder.LABEL,
                     'Text': {'fr': 'Langue console',
                              'en': 'Consol Language',
                              },
                     },
                    {'Type': QBuilder.COMBOBOX,
                     'Bulle': {'fr': 'Choix de la langue',
                               'en': 'Choice of language',
                               },
                     'Choices': kLangague.getAllLangague,
                     'Check': kLangague.getPosLangague(),
                     'Action': kLangague.setLangague,
                     },
                ],
            },
            {'Type': QBuilder.BOX_LAYOUT,
             'Orientation': QBuilder.VERTICAL,
             'Contenu': [
                 {'Type': QBuilder.LABEL,
                  'Text': {'fr': 'label fr',
                           'en': 'label en',
                           },
                  'Font': {'Size': 10, 'Bold': True, },
                  },
                 {'Type': QBuilder.LABEL,
                  'Text': {'fr': '<font color=\"#DF0101\">label rouge fr</font>',
                           'en': '<font color=\"#DF0101\">label red en</font>',
                           },
                  'Font': {'Bold': True, },
                  },
             ]
             },
        ],
    }

    console = dict()
    console.update({'TITLE': 'QBuilder (1.0)', }, )
    console.update({'Langague': kLangague.getSigleLangague(), }, )
    console.update({'MENU': console_menu, }, )
    console.update({'LAYOUT': console_layout, }, )

    qbuilder(console, [kLangague, kMenu], lambda: kernel.run())


if __name__ == "__main__":
    main()
