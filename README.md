# QBuilder

- [FR] Facilitateur de construction d'IHM QT5 multi-langues 
- [EN] Facilitator of construction of IHM QT5 multi-languages

Copyright (c) 2012, Jacques-Bernard Lekien - The Practice Astronom Guide Book Project

Copyright (c) 2018, extended graphic widget for AstroNoot

Copyright (c) 2020, release version for industrialization in LivingStars

TODO
- Faire un test avec Link, appel direct aux méthodes
- Faire un test avec Save
