# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def builder_box_layout(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        GUI_KEY_TYPE: GUI_BOX_LAYOUT,
    Optional configuration:
        GUI_KEY_ORIENTATION = BUILDER_HORIZONTAL | BUILDER_VERTICAL
        GUI_KEY_CONTENT = [ ... ] | ...
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_BOX_LAYOUT:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_BOX_LAYOUT + ':')
    orientation = GUI_VERTICAL
    try:
        orientation = conf[GUI_KEY_ORIENTATION]
    except KeyError:
        pass
    if orientation == GUI_HORIZONTAL:
        builder.noisy('  Orientation: ' + GUI_HORIZONTAL)
        if isinstance(parent, QtWidgets.QWidget):
            # Case : parent is widget
            q_layout: QtWidgets.QBoxLayout = QtWidgets.QHBoxLayout(parent)
        else:
            # Case : parent is layout
            q_layout: QtWidgets.QBoxLayout = QtWidgets.QHBoxLayout()
    elif orientation == GUI_VERTICAL:
        builder.noisy('  Orientation: ' + GUI_VERTICAL)
        if isinstance(parent, QtWidgets.QWidget):
            # Case : parent is widget
            q_layout: QtWidgets.QBoxLayout = QtWidgets.QVBoxLayout(parent)
        else:
            # Case : parent is layout
            q_layout: QtWidgets.QBoxLayout = QtWidgets.QVBoxLayout()
    else:
        builder.warning('  Orientation: ' + conf[GUI_KEY_ORIENTATION] + ' (' + GUI_HORIZONTAL + ' ou ' + GUI_VERTICAL + ')')
        return True
    if isinstance(parent, QtWidgets.QLayout):
        parent.addLayout(q_layout)
    try:
        for conf in conf[GUI_KEY_CONTENT]:
            builder.interpreter(q_layout, conf)
    except KeyError:
        try:
            builder.interpreter(q_layout, conf[GUI_KEY_CONTENT])
        except KeyError:
            pass
    return True
