# -*- coding: utf-8 -*-
from builder_const import *

import os
from PyQt5 import QtWidgets, QtGui


def builder_button(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_BUTTON,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_BUTTON:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_BUTTON + ':')
    try:
        button = QtWidgets.QPushButton(builder.text(conf[GUI_KEY_TEXT]))
        builder.noisy('   ' + GUI_KEY_TEXT + ': ' + builder.text(conf[GUI_KEY_TEXT]))
        builder.append(
            lambda b=builder, l=button, text=conf[GUI_KEY_TEXT]: l.setText(b.text(text))
        )
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_TEXT)
        button = QtWidgets.QPushButton()
    try:
        path = os.getcwd()
        # ne marche pas button.setStyleSheet('background-image: url(' + path + '\\' + conf[GUI_KEY_ICON] + ');')
        icon = QtGui.QIcon(path + '\\' + conf[GUI_KEY_ICON])
        button.setIcon(icon)
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_ICON)
    try:
        button.clicked.connect(conf[GUI_KEY_ACTION])
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_ACTION)
    try:
        button.setEnabled(conf[GUI_KEY_ENABLED])
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_ENABLED)
    # remplaced by builder_font(button, conf) ??
    try:
        button.setStyleSheet(conf[GUI_KEY_STYLESHEET])
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_STYLESHEET)
    builder_tool_tip(builder, button, conf)
    method = {GUI_KEY_TEXT: button.setText, GUI_KEY_ENABLED: button.setEnabled,
              GUI_KEY_STYLESHEET: button.setStyleSheet}
    builder_link(conf, method)
    # ??
    try:
        save = conf[GUI_KEY_SAVE]
        save[0](save[1], button)
    except KeyError:
        pass
    parent.addWidget(button)
    return True
