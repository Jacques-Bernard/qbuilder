# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def change_lang_combobox(builder, combobox, all_choices):
    choices = builder.text(all_choices)
    for i, value in enumerate(choices):
        combobox.setItemText(i, value)


def builder_combobox(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_COMBOBOX,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    """
    ('ComboBox',
     {'Title':
      'Bulle':
      'Choices':
      'Check':
      'Action':
      'Save':
     }
     [...]
    )
    """
    if conf[GUI_KEY_TYPE] != GUI_COMBOBOX:
        return False
    builder.noisy(GUI_COMBOBOX + ':')
    #
    group_box = builder_group_box(builder, conf)
    combobox = QtWidgets.QComboBox()
    choices = builder.text(conf[GUI_KEY_CHOICES])
    combobox.addItems(choices)
    builder.append(
        lambda b=builder, c=combobox, ch=conf[GUI_KEY_CHOICES]: change_lang_combobox(b, c, ch)
    )
    try:
        combobox.currentIndexChanged.connect(lambda x: conf[GUI_KEY_ACTION](x, choices[x]))
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_ACTION)
    if isinstance(conf[GUI_KEY_CHECK], int):
        combobox.setCurrentIndex(conf[GUI_KEY_CHECK])
    else:
        combobox.setCurrentIndex(combobox.findText(builder.text(conf[GUI_KEY_CHECK])))
    #
    try:
        save = conf[GUI_KEY_SAVE]
        save[0](save[1], combobox)
    except KeyError:
        pass
    #
    try:
        if conf[GUI_KEY_ORIENTATION] == GUI_VERTICAL:
            layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
        elif conf[GUI_KEY_ORIENTATION] == GUI_HORIZONTAL:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        else:
            builder.warning(
                '  ' + GUI_KEY_ORIENTATION + ': ' + conf[
                    GUI_KEY_ORIENTATION] + ' (' + GUI_HORIZONTAL + ' ou ' + GUI_VERTICAL + ')')
            return False
    except KeyError:
        layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
    layout.addWidget(combobox)
    # layout.addStretch(1)
    if group_box:
        builder_tool_tip(builder, group_box, conf)
        group_box.setLayout(layout)
        parent.addWidget(group_box)
    else:
        builder_tool_tip(builder, combobox, conf)
        parent.addWidget(combobox)
    method = {GUI_KEY_TEXT: lambda text, c=combobox: c.setCurrentIndex(c.findText(text)),
              GUI_KEY_INDEX: combobox.setCurrentIndex,
              GUI_KEY_ENABLED: combobox.setEnabled}
    builder_link(conf, method)
    return True
