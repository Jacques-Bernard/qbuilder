# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def selected_text_in_checkboxes(text, checkboxes):
    for checkbox in checkboxes:
        checkbox.setChecked(checkbox.getText() == text)


def selected_index_in_checkboxes(index, checkboxes):
    for i, checkbox in enumerate(checkboxes):
        checkbox.setChecked(i == index)


def selected_all(index, checkboxes):
    for i, checkbox in enumerate(checkboxes):
        checkbox.setChecked(True)


def selected_none(index, checkboxes):
    for i, checkbox in enumerate(checkboxes):
        checkbox.setChecked(False)


def builder_combobox_check(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_COMBOBOX,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    """
    ('ComboBox',
     {'Title':
      'Bulle':
      'Choices':
      'Check':
      'Action':
      'Save':
     }
     [...]
    )
    """
    if conf[GUI_KEY_TYPE] != GUI_COMBOBOX_CHECK:
        return False
    builder.noisy(GUI_COMBOBOX_CHECK + ':')
    #
    group_box = builder_group_box(builder, conf)
    #
    choices = builder.text(conf[GUI_KEY_CHOICES])
    number_columns = -1
    i_column = 0
    i_line = 0
    try:
        if conf[GUI_KEY_ORIENTATION] == GUI_GRID:
            layout: QtWidgets.QLayout = QtWidgets.QGridLayout()
            number_columns = conf[GUI_KEY_GRID_COLUMNS]
        elif conf[GUI_KEY_ORIENTATION] == GUI_VERTICAL:
            layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
        elif conf[GUI_KEY_ORIENTATION] == GUI_HORIZONTAL:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        else:
            builder.warning(
                '  ' + GUI_KEY_ORIENTATION + ': ' + conf[
                    GUI_KEY_ORIENTATION] + ' (' + GUI_HORIZONTAL + ' ou ' + GUI_VERTICAL + ')')
            return False
    except KeyError:
        layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
    checkboxes = []
    for index, (choice, check) in enumerate(zip(choices, conf[GUI_KEY_CHECKES])):
        checkbox = QtWidgets.QCheckBox(choice)
        builder.append(
            lambda b=builder, c=checkbox, i=index: c.setText(b.text(conf[GUI_KEY_CHOICES])[i])
        )
        checkbox.setChecked(check)
        checkbox.toggled.connect(lambda s, i=index: conf[GUI_KEY_ACTION](s, i))
        builder_tool_tip(builder, checkbox, conf, index)
        checkboxes.append(checkbox)
        if number_columns != -1:
            layout.addWidget(checkbox, i_line, i_column)
            i_column += 1
            if i_column == number_columns:
                i_column = 0
                i_line += 1
        else:
            layout.addWidget(checkbox)
    if number_columns == -1:
        layout.addStretch(1)
    if group_box:
        group_box.setLayout(layout)
        parent.addWidget(group_box)
    else:
        parent.addWidget(layout)
    method = {GUI_KEY_TEXT: lambda text, c=checkboxes: selected_text_in_checkboxes(text, c),
              GUI_KEY_INDEX: lambda index, c=checkboxes: selected_index_in_checkboxes(index, c),
              GUI_KEY_ALL: lambda c=checkboxes: selected_all(c),
              GUI_KEY_NONE: lambda c=checkboxes: selected_none(c),
              GUI_KEY_ENABLED: layout.setEnabled}
    builder_link(conf, method)
    return True
