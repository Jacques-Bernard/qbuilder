# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def selected_text_in_radiobutton(text, radiobutton):
    for radiobutton in radiobutton:
        radiobutton.setChecked(radiobutton.getText() == text)


def selected_index_in_radiobutton(index, radiobutton):
    for i, radiobutton in enumerate(radiobutton):
        radiobutton.setChecked(i == index)


def builder_combobox_radio(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_COMBOBOX,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    """
    ('ComboBox',
     {'Title':
      'Bulle':
      'Choices':
      'Check':
      'Action':
      'Save':
     }
     [...]
    )
    """
    if conf[GUI_KEY_TYPE] != GUI_COMBOBOX_RADIO:
        return False
    builder.noisy(GUI_COMBOBOX_RADIO + ':')
    #
    group_box = builder_group_box(builder, conf)
    #
    choices = builder.text(conf[GUI_KEY_CHOICES])
    if isinstance(conf[GUI_KEY_CHECK], int):
        check = choices[conf[GUI_KEY_CHECK]]
    else:
        check = builder.text(conf[GUI_KEY_CHECK])
        if check not in choices:
            # TODO mettre un message
            pass
    try:
        if conf[GUI_KEY_ORIENTATION] == GUI_VERTICAL:
            layout: QtWidgets.QLayout = QtWidgets.QVBoxLayout()
        elif conf[GUI_KEY_ORIENTATION] == GUI_HORIZONTAL:
            layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
        else:
            builder.warning(
                '  ' + GUI_KEY_ORIENTATION + ': ' + conf[
                    GUI_KEY_ORIENTATION] + ' (' + GUI_HORIZONTAL + ' ou ' + GUI_VERTICAL + ')')
            return False
    except KeyError:
        layout: QtWidgets.QLayout = QtWidgets.QHBoxLayout()
    radiobuttons = []
    for index, choice in enumerate(choices):
        radiobutton = QtWidgets.QRadioButton(choice)
        builder.append(
            lambda b=builder, c=radiobutton, i=index: c.setText(b.text(conf[GUI_KEY_CHOICES])[i])
        )
        radiobutton.setChecked(check == choice)
        radiobutton.toggled.connect(lambda s, i=index: conf[GUI_KEY_ACTION](s, i))
        builder_tool_tip(builder, radiobutton, conf, index)
        radiobuttons.append(radiobutton)
        layout.addWidget(radiobutton)
    layout.addStretch(1)
    if group_box:
        group_box.setLayout(layout)
        parent.addWidget(group_box)
    else:
        parent.addWidget(layout)
    method = {GUI_KEY_TEXT: lambda text, r=radiobuttons: selected_text_in_radiobutton(text, r),
              GUI_KEY_INDEX: lambda index, r=radiobuttons: selected_index_in_radiobutton(index, r),
              GUI_KEY_ENABLED: layout.setEnabled}
    builder_link(conf, method)
    return True
