# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets

GUI_BOX_LAYOUT = 'Box_Layout'
GUI_HORIZONTAL = 'H'
GUI_VERTICAL = 'V'
GUI_GRID = 'G'
GUI_LABEL = 'Label'
GUI_COMBOBOX = 'Combobox'
GUI_COMBOBOX_RADIO = 'Combobox_Radio'
GUI_COMBOBOX_CHECK = 'Combobox_Check'
GUI_BUTTON = 'Button'
GUI_LINE_EDIT = 'Line_Edit'
GUI_FIGURE = 'Figure'
GUI_TEXT_EDIT = 'Text_Edit'
GUI_IMAGE = 'Image'

GUI_SEPARATOR = 'Separator'

GUI_KEY_TITLE = 'Title'
GUI_KEY_TYPE = 'Type'
GUI_KEY_ORIENTATION = 'Orientation'
GUI_KEY_CONTENT = 'Content'
GUI_KEY_CHOICES = 'Choices'
GUI_KEY_ACTION = 'Action'
GUI_KEY_TOOL_TIP = 'ToolTip'
GUI_KEY_CHECK = 'Check'
GUI_KEY_CHECKES = 'Checkes'
GUI_KEY_SAVE = 'Save'
GUI_KEY_TEXT = 'Text'
GUI_KEY_FONT = 'Font'
GUI_KEY_SIZE = 'Size'
GUI_KEY_BOLD = 'Bold'
GUI_KEY_LINK = 'Link'
GUI_KEY_ENABLED = 'Enabled'
GUI_KEY_INDEX = 'Index'
GUI_KEY_STYLESHEET = 'StyleSheet'
GUI_KEY_SAVE = 'Save'
GUI_KEY_GRID_COLUMNS = 'GridColumns'
GUI_KEY_ALL = 'All'
GUI_KEY_NONE = 'None'
GUI_KEY_ICON = 'Icon'
GUI_KEY_SOURCE = 'Source'
GUI_KEY_WIDTH = 'Width'
GUI_KEY_HEIGHT = 'Height'
GUI_KEY_CLASS_FIGURE = 'ClassFigure'


def builder_font(widget: QtWidgets.QWidget, conf: dict):
    font = widget.font()
    try:
        font.setPointSize(conf[GUI_KEY_FONT][GUI_KEY_SIZE])
    except KeyError:
        pass
    try:
        font.setBold(conf[GUI_KEY_FONT][GUI_KEY_BOLD])
    except KeyError:
        pass
    widget.setFont(font)


def builder_tool_tip(builder, widget: QtWidgets.QWidget, conf: dict, index: int = 0):
    try:
        if isinstance(conf[GUI_KEY_TOOL_TIP], list):
            tool_tip = conf[GUI_KEY_TOOL_TIP][index]
        else:
            tool_tip = conf[GUI_KEY_TOOL_TIP]
        widget.setToolTip(builder.text(tool_tip))
        builder.noisy('   ' + GUI_KEY_TOOL_TIP + ': ' + builder.text(tool_tip))
        builder.append(
            lambda b=builder, l=widget, text=tool_tip: l.setToolTip(b.text(text))
        )
    except KeyError:
        pass


def builder_link(conf: dict, method: dict):
    """
    conf[GUI_KEY_LINK] = () | [()]
        () = (?, GUI_KEY_?, ?)
    :param conf:
    :param method:
    :return:
    """
    try:
        links = conf[GUI_KEY_LINK]
        if not isinstance(links, list):
            links = [links]
        for key in [GUI_KEY_TEXT, GUI_KEY_INDEX, GUI_KEY_ENABLED]:
            try:
                if links[1] == key:
                    links[0](links[2], method[key])
            except KeyError:
                pass
    except KeyError:
        pass


def builder_group_box(builder, conf):
    try:
        group_box: QtWidgets.QGroupBox = QtWidgets.QGroupBox(builder.text(conf[GUI_KEY_TITLE]))
        builder.noisy('   ' + GUI_KEY_TITLE + ': ' + builder.text(conf[GUI_KEY_TITLE]))
        group_box.setFlat(False)
        builder.append(
            lambda b=builder, g=group_box, l=conf[GUI_KEY_TITLE]: g.setTitle(b.text(l))
        )
        return group_box
    except KeyError:
        return None
