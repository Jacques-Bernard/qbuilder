# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets, QtGui


def builder_figure(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_FIGURE,
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_FIGURE:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_FIGURE + ':')
    figure = conf[GUI_KEY_CLASS_FIGURE](builder.main_widget)
    builder_tool_tip(builder, figure, conf)
    parent.addWidget(figure)
    return True
