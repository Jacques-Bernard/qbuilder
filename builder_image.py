# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets, QtGui


def builder_image(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_IMAGE,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_IMAGE:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_IMAGE + ':')
    try:
        image: QtWidgets.QLabel = QtWidgets.QLabel()
        pixels_map: QtGui.QPixmap = QtGui.QPixmap(conf[GUI_KEY_SOURCE])
        image.setPixmap(pixels_map)
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_SOURCE)
        return True
    try:
        width = conf[GUI_KEY_WIDTH]
        try:
            height = conf[GUI_KEY_HEIGHT]
        except KeyError:
            if not pixels_map.width():
                builder.noisy('  ' + GUI_KEY_SOURCE + ' not found')
                return True
            height = int((pixels_map.height() * width) / pixels_map.width())
    except KeyError:
        try:
            if not pixels_map.height():
                builder.noisy('  ' + GUI_KEY_SOURCE + ' not found')
                return True
            height = conf[GUI_KEY_HEIGHT]
            width = int((pixels_map.width() * height) / pixels_map.height())
        except KeyError:
            width = pixels_map.width()
            height = pixels_map.height()
    pixels_map.scaled(width, height)
    image.setScaledContents(True)
    image.setFixedSize(width, height)
    builder_tool_tip(builder, image, conf)
    parent.addWidget(image)
    return True
