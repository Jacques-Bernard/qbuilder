# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def builder_label(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_LABEL,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_LABEL:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_LABEL + ':')
    try:
        label: QtWidgets.QLabel = QtWidgets.QLabel(builder.text(conf[GUI_KEY_TEXT]))
        builder.noisy('   ' + GUI_KEY_TEXT + ': ' + builder.text(conf[GUI_KEY_TEXT]))
        builder.append(
            lambda b=builder, l=label, text=conf[GUI_KEY_TEXT]: l.setText(b.text(text))
        )
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_TEXT)
        return True
    builder_font(label, conf)
    builder_tool_tip(builder, label, conf)
    method = {GUI_KEY_TEXT: label.setText, GUI_KEY_ENABLED: label.setEnabled}
    builder_link(conf, method)
    parent.addWidget(label)
    return True
