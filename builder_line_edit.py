# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def set_text(builder, line_edit, text):
    if line_edit.displayText() != builder.text(text):
        line_edit.setText(builder.text(text))


def builder_line_edit(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_LINE_EDIT,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Action' = method(text: in str)
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_LINE_EDIT:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_LINE_EDIT + ':')
    try:
        line_edit: QtWidgets.QLabel = QtWidgets.QLineEdit(builder.text(conf[GUI_KEY_TEXT]))
        builder.noisy('   ' + GUI_KEY_TEXT + ': ' + builder.text(conf[GUI_KEY_TEXT]))
        builder.append(
            lambda b=builder, l=line_edit, text=conf[GUI_KEY_TEXT]: set_text(b, l, text)
        )
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_TEXT)
    try:
        line_edit.editingFinished.connect(lambda l=line_edit: conf[GUI_KEY_ACTION](l.displayText()))
    except KeyError:
        pass
    builder_font(line_edit, conf)
    builder_tool_tip(builder, line_edit, conf)
    method = {GUI_KEY_TEXT: line_edit.setText, GUI_KEY_ENABLED: line_edit.setEnabled}
    builder_link(conf, method)
    parent.addWidget(line_edit)
    return True
