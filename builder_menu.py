# -*- coding: utf-8 -*-
"""

"""
from builder_const import *
from PyQt5 import QtWidgets, QtCore, QtGui

clavier = dict()
clavier['CTRL+A'] = QtCore.Qt.CTRL + QtCore.Qt.Key_A
clavier['CTRL+D'] = QtCore.Qt.CTRL + QtCore.Qt.Key_D
clavier['CTRL+E'] = QtCore.Qt.CTRL + QtCore.Qt.Key_E
clavier['CTRL+O'] = QtCore.Qt.CTRL + QtCore.Qt.Key_O
clavier['CTRL+Q'] = QtCore.Qt.CTRL + QtCore.Qt.Key_Q
clavier['CTRL+S'] = QtCore.Qt.CTRL + QtCore.Qt.Key_S


def builder_menu(builder, conf: dict):
    """
    Menu Mandatory configuration:
        'MENU' = list('...' | { 'fr': '...', 'en':..., }
        'Font': { 'Size' : #
                  'Bold' : True | False
        'Link': ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param conf: configuration
    """
    menu_bar = QtWidgets.QMenuBar()
    try:
        for menu in conf['MENU']:
            builder.noisy('MENU ' + builder.text(menu[0]) + ' (' + str(menu[0]) + ')')
            q_menu = QtWidgets.QMenu(builder.text(menu[0]), builder)
            for sub_menu in menu[1]:
                if isinstance(sub_menu, str):
                    if sub_menu == GUI_SEPARATOR:
                        q_menu.addSeparator()
                        builder.noisy('MENU ' + builder.text(menu[0]) + GUI_SEPARATOR)
                        continue
                    builder.warning('builder_menu', 'MENU ' + builder.text(menu[0]) + ' don\'t understand ' + str(
                        sub_menu) + ' in ' + str(menu[1]))
                    continue
                name = builder.text(sub_menu[0])
                action = builder.close
                # action = sub_menu[1]
                try:
                    key = clavier[builder.text(sub_menu[2])]
                    q_menu.addAction(name, action, QtGui.QKeySequence(key))
                    builder.noisy('MENU ' + builder.text(menu[0]) + '/' + name + ' ' + str(action) + ' ' + str(key))
                except IndexError:
                    q_menu.addAction(name, action)
                    builder.noisy('MENU ' + builder.text(menu[0]) + '/' + name + ' ' + str(action))
            menu_bar.addMenu(q_menu)
        builder.setMenuBar(menu_bar)
    except KeyError:
        builder.noisy('No MENU in configuration')
