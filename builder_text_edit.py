# -*- coding: utf-8 -*-
from builder_const import *

from PyQt5 import QtWidgets


def set_text(builder, text_edit, text):
    if text_edit.toPlainText() != builder.text(text):
        text_edit.setPlainText(builder.text(text))


def builder_text_edit(builder, parent: QtWidgets.QWidget, conf: dict):
    """
    Label Mandatory configuration:
        'Type': GUI_LINE_EDIT,
    Optional configuration:
        'Text' = '...' | { 'fr': '...', 'en':..., }
        'Font' = { 'Size' : #
                  'Bold' : True | False
        'Action' = method(text: in str)
        'Link' = ( 'Text' | 'Enabled', fct )
    :param builder: QBuilder instance
    :param parent: parent widget
    :param conf: configuration
    :return: Returns True if the configuration matches a QBuilder.LABEL
    """
    if conf[GUI_KEY_TYPE] != GUI_TEXT_EDIT:
        return False
    builder.noisy(GUI_KEY_TYPE + ' ' + GUI_TEXT_EDIT + ':')
    try:
        text_edit: QtWidgets.QLabel = QtWidgets.QTextEdit()
        set_text(builder, text_edit, conf[GUI_KEY_TEXT])
        builder.noisy('   ' + GUI_KEY_TEXT + ': ' + builder.text(conf[GUI_KEY_TEXT]))
        builder.append(
            lambda b=builder, t=text_edit, text=conf[GUI_KEY_TEXT]: set_text(b, t, text)
        )
    except KeyError:
        builder.noisy('  not attribute ' + GUI_KEY_TEXT)
        text_edit: QtWidgets.QLabel = QtWidgets.QTextEdit()
    try:
        text_edit.textChanged.connect(lambda t=text_edit, text=conf[GUI_KEY_ACTION]: text(t.toPlainText()))
    except KeyError:
        pass
    builder_font(text_edit, conf)
    builder_tool_tip(builder, text_edit, conf)
    method = {GUI_KEY_TEXT: text_edit.setText, GUI_KEY_ENABLED: text_edit.setEnabled}
    builder_link(conf, method)
    parent.addWidget(text_edit)
    return True
