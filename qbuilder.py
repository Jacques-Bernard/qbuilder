# -*- coding: utf-8 -*-
"""

"""
from builder_box_layout import builder_box_layout
from builder_button import builder_button
from builder_combobox import builder_combobox
from builder_combobox_radio import builder_combobox_radio
from builder_combobox_check import builder_combobox_check
from builder_figure import builder_figure
from builder_image import builder_image
from builder_label import builder_label
from builder_line_edit import builder_line_edit
from builder_text_edit import builder_text_edit

from builder_menu import builder_menu

from PyQt5 import QtWidgets, QtCore

clavier = dict()
clavier['CTRL+A'] = QtCore.Qt.CTRL + QtCore.Qt.Key_A
clavier['CTRL+D'] = QtCore.Qt.CTRL + QtCore.Qt.Key_D
clavier['CTRL+E'] = QtCore.Qt.CTRL + QtCore.Qt.Key_E
clavier['CTRL+O'] = QtCore.Qt.CTRL + QtCore.Qt.Key_O
clavier['CTRL+Q'] = QtCore.Qt.CTRL + QtCore.Qt.Key_Q
clavier['CTRL+S'] = QtCore.Qt.CTRL + QtCore.Qt.Key_S


class QBuilder(QtWidgets.QMainWindow):
    language_actions = []
    default_language = 'fr'

    def append(self, language_action):
        self.language_actions.append(language_action)

    def noisy(self, msg: str):
        """
        Method to display trace
        :param msg: message
        """
        if self.verbose:
            print(msg)

    def warning(self, fct: str, msg: str):
        """
        Method to display warning
        :param fct: function name
        :param msg: message
        """
        if self.verbose:
            print('WARNING(' + fct + ') ' + msg)

    def not_exist_translation(self, method, acronym, msg):
        if msg is None:
            self.warning(method, 'No exist a translation (still less in ' + acronym + ')')
        else:
            self.warning(method, 'Not exist a translation in ' + acronym + ' for \'' + msg + '\'')

    def text(self, conf):
        """
        If the configuration is a dictionary, it returns the text corresponding
        to the key to the common language.
        Otherwise it returns the configuration which is then a text.
        :param conf: string or dictionary or method
        :return: string or list(string)
        """
        if isinstance(conf, str):
            return conf
        if isinstance(conf, dict):
            try:
                return conf[self.lang]
            except KeyError:
                try:
                    msg = conf[self.default_language]
                    self.not_exist_translation('QBuilder.text', self.lang, str(conf[self.default_language]))
                    return msg
                except KeyError:
                    try:
                        msg = conf[conf.keys[0]]
                        self.not_exist_translation('QBuilder.text', self.lang, str(conf[conf.keys[0]]))
                        return msg
                    except KeyError:
                        self.not_exist_translation('QBuilder.text', self.lang, None)
                        return ''
        try:
            return conf(self.lang)
        except (KeyError, NameError, TypeError):
            try:
                msg = conf(self.default_language)
                self.not_exist_translation('QBuilder.text', self.lang, str(msg))
                return msg
            except (KeyError, NameError, TypeError):
                try:
                    return conf()
                except (KeyError, NameError, TypeError):
                    self.not_exist_translation('QBuilder.text', self.lang, None)
                    return ''

    def interpreter(self, parent, conf):
        """
        Interpreter the current configuration
        :param parent: parent widget
        :param conf: configuration
        """
        if builder_box_layout(self, parent, conf):
            return
        if builder_button(self, parent, conf):
            return
        if builder_combobox(self, parent, conf):
            return
        if builder_combobox_radio(self, parent, conf):
            return
        if builder_combobox_check(self, parent, conf):
            return
        if builder_figure(self, parent, conf):
            return
        if builder_image(self, parent, conf):
            return
        if builder_label(self, parent, conf):
            return
        if builder_line_edit(self, parent, conf):
            return
        if builder_text_edit(self, parent, conf):
            return

    def action_language(self, lang):
        self.lang = lang
        for widget in self.language_actions:
            widget()

    @property
    def main_widget(self):
        return self.main

    def __init__(self, conf, kernels):
        self.verbose = True
        for kernel in kernels:
            kernel.set(self)
        try:
            self.lang = conf['LANGUAGE']
            self.noisy('LANGUAGE ' + self.lang)
        except KeyError:
            self.lang = None
            self.noisy('No LANGUAGE in configuration')
        QtWidgets.QMainWindow.__init__(self)
        try:
            self.setWindowTitle(conf['TITLE'])
            self.noisy('TITLE ' + conf['TITLE'])
        except KeyError:
            self.noisy('No TITLE in configuration')
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        builder_menu(self, conf)
        self.main = QtWidgets.QWidget(self)
        self.main.setContentsMargins(0, 0, 0, 0)
        try:
            self.interpreter(self.main, conf['LAYOUT'])
        except KeyError:
            self.noisy('No LAYOUT in configuration')
        #
        self.main.setFocus()
        self.setCentralWidget(self.main)
        # to center the positioning of the application
        self.show()
        widget = QtWidgets.QApplication.desktop()
        desktop_width = widget.width()
        desktop_height = widget.height()
        x = desktop_width / 2 - self.width() / 2
        y = desktop_height / 2 - self.height() / 2
        if y > 25:
            y = y - 25
        self.move(QtCore.QPoint(x, y))


def q_builder(console, kernels, reset):
    import sys
    application = QtWidgets.QApplication(sys.argv)
    builder = QBuilder(console, kernels)
    reset()
    builder.show()
    sys.exit(application.exec_())
