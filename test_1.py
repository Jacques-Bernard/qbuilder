# -*- coding: utf-8 -*-
"""
Use case
"""
from builder_const import *
from qbuilder import QBuilder, q_builder

from test_kernel_language import kLanguage

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
import pylab
import numpy as np
import matplotlib.pyplot as plt


class ClassFigure(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=200):
        w, h = pylab.figaspect(1.)
        fig = pylab.figure(figsize=(w/2, h/2), dpi=100)
        self.fig = fig
        self.ax = fig.add_subplot(111)
        self.axes = fig.gca()
        FigureCanvasQTAgg.__init__(self, fig)
        self.setParent(parent)
        FigureCanvasQTAgg.setSizePolicy(self,
                                        QtWidgets.QSizePolicy.Fixed,
                                        QtWidgets.QSizePolicy.Fixed)
        FigureCanvasQTAgg.updateGeometry(self)
        self.update_figure()

    def update_figure(self):
        pylab.xlim(xmin=-1.0, xmax=2.0)
        pylab.ylim(ymin=-1.0, ymax=2.0)
        # pylab.tight_layout()
        pylab.grid(True)
        pylab.grid(True)
        plt.title('square and circle')
        plt.xlabel('x-coordinates')
        plt.ylabel('y-coordinates')
        x = np.array([0, 1, 1, 0, 0])
        y = np.array([0, 0, 1, 1, 0])
        plt.plot(x, y, label='square')
        theta = np.linspace(0, 2 * np.pi, 40)
        x = np.cos(theta) + 0.5
        y = np.sin(theta) + 0.5
        plt.plot(x, y, label='circle')
        plt.legend()
        self.draw()


class Kernel:
    def __init__(self):
        self.builder = None
        self.my_line_edit_default = {
            'fr': 'GUI_LINE_EDIT texte fr',
            'en': 'GUI_LINE_EDIT text en',
        }
        self.my_line_edit = None
        self.my_button = {
            'fr': 'GUI_BUTTON texte fr',
            'en': 'GUI_BUTTON text en',
        }
        self.my_combobox = {
            'fr': ['GUI_COMBOBOX choix 0', 'GUI_COMBOBOX choix 1', 'GUI_COMBOBOX choix 2', 'GUI_COMBOBOX choix 3',
                   'GUI_COMBOBOX choix 4', 'GUI_COMBOBOX choix 5', ],
            'en': ['GUI_COMBOBOX choice 0', 'GUI_COMBOBOX choice 1', 'GUI_COMBOBOX choice 2', 'GUI_COMBOBOX choice 3',
                   'GUI_COMBOBOX choice 4', 'GUI_COMBOBOX choice 5', ],
        }
        self.my_combobox_index = 2
        self.my_combobox_radio = {
            'fr': ['Rouge', 'Vert', 'Bleu', ],
            'en': ['Red', 'Green', 'Blue', ],
        }
        self.my_combobox_radio_index = 1
        self.my_combobox_check = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', ]
        self.my_combobox_check_checkes = [True, False, True, True, False, False, True, False]
        self.my_text_edit_default = {
            'fr': 'GUI_TEXT_EDIT texte fr',
            'en': 'GUI_TEXT_EDIT text en',
        }
        self.my_text_edit = None
        self.my_button_icon = {
            'fr': 'GUI_BUTTON icône fr',
            'en': 'GUI_BUTTON icon en',
        }

    #  -------

    def set(self, builder: QBuilder):
        """
        Mandatory method for q_builder
        Memorization in order to be able to call change_language.
        :param builder:
        :return:
        """
        self.builder = builder

    #  -------

    def get_line_edit(self, acronym):
        if self.my_line_edit is None:
            return self.my_line_edit_default[acronym]
        for my_line_edit_default in self.my_line_edit_default.values():
            if self.my_line_edit == my_line_edit_default:
                return self.my_line_edit_default[acronym]
        return self.my_line_edit

    def set_line_edit(self, text):
        if self.my_line_edit == text:
            return
        print('run Action ' + GUI_LINE_EDIT + ' ' + text)
        self.my_line_edit = text

    #  -------

    def get_text_button(self, acronym):
        return self.my_button[acronym]

    @staticmethod
    def clicked_button(self):
        print('run Action clicked (button text)')

    #  -------

    def get_my_combobox(self, acronym):
        return self.my_combobox[acronym]

    def get_my_combobox_index(self):
        return self.my_combobox_index

    def set_my_combobox_index(self, ind, text):
        if self.my_combobox_index == ind:
            return
        print('run Action ' + GUI_COMBOBOX + ' ' + str(ind) + ' ' + text)
        self.my_combobox_index = ind

    #  -------

    def get_my_combobox_radio(self, acronym):
        return self.my_combobox_radio[acronym]

    def get_my_combobox_radio_index(self):
        return self.my_combobox_radio_index

    def set_my_combobox_radio_index(self, state, index):
        """
        One call with a false state to indicate that the deselecting of
        one call with a true state to indicate the new selection.
        :param state:
        :param index:
        :return:
        """
        print('run Action ' + GUI_COMBOBOX_RADIO + ' ' + str(state) + ' ' + str(index))
        # Here just memorize the index selected
        if state:
            self.my_combobox_radio_index = index
            print('memorize ' + GUI_COMBOBOX_RADIO + ' ' + str(index) + ' ' + str(self.my_combobox_radio['fr'][index]))

    #  -------

    def get_my_combobox_check(self):
        return self.my_combobox_check

    def get_my_combobox_check_checkes(self):
        return self.my_combobox_check_checkes

    def set_my_combobox_check_index(self, state, index):
        """
        One call with a false state to indicate that the deselecting of
        one call with a true state to indicate the new selection.
        :param state:
        :param index:
        :return:
        """
        print('run Action ' + GUI_COMBOBOX_CHECK + ' ' + str(state) + ' ' + str(index))
        self.my_combobox_check_checkes[index] = state

    #  -------

    def get_text_edit(self, acronym):
        if self.my_text_edit is None:
            return self.my_text_edit_default[acronym]
        for my_text_edit_default in self.my_text_edit_default.values():
            if self.my_text_edit == my_text_edit_default:
                return self.my_text_edit_default[acronym]
        return self.my_text_edit

    def set_text_edit(self, text):
        if self.my_text_edit == text:
            return
        print('run Action ' + GUI_TEXT_EDIT + ' ' + text)
        self.my_text_edit = text

    #  -------

    def get_text_button_icon(self, acronym):
        return self.my_button_icon[acronym]

    @staticmethod
    def clicked_button_icon(self):
        print('run Action clicked (button icon)')

    #  -------

    def run(self):
        pass


kernel = Kernel()


class KernelMenu:
    _application = None

    def quit(self):
        self._application.close()

    def set(self, application):
        """
        Memorisation de l'application afin de pouvoir appeler changeLangague.
        :param application:
        :return:
        """
        self._application = application


kMenu = KernelMenu()

menu = [
    (
        {
            'fr': '&Fichier',
            'en': '&File',
        },
        [
            (
                {
                    'fr': '&Quitter',
                    'en': '&Quit',
                },
                kMenu.quit,
                {
                    'fr': 'CTRL+Q',
                    'en': 'CTRL+Q',
                },
            ),
        ],
    ),
]

layout = {
    GUI_KEY_TYPE: GUI_BOX_LAYOUT,
    GUI_KEY_ORIENTATION: GUI_VERTICAL,
    GUI_KEY_CONTENT: [
        {
            GUI_KEY_TYPE: GUI_BOX_LAYOUT,
            GUI_KEY_ORIENTATION: GUI_HORIZONTAL,
            GUI_KEY_CONTENT: [
                {
                    GUI_KEY_TYPE: GUI_LABEL,
                    GUI_KEY_TEXT: {
                        'fr': 'Langue de la console',
                        'en': 'Consol Language',
                    },
                },
                {
                    GUI_KEY_TYPE: GUI_COMBOBOX,
                    GUI_KEY_TOOL_TIP: {
                        'fr': 'Choix de la langue de la console',
                        'en': 'Choice of consol language',
                    },
                    GUI_KEY_CHOICES: kLanguage.get_languages,
                    GUI_KEY_CHECK: kLanguage.get_language_index(),
                    GUI_KEY_ACTION: kLanguage.set_language_index
                },
            ],
        },
        {
            GUI_KEY_TYPE: GUI_BOX_LAYOUT,
            GUI_KEY_ORIENTATION: GUI_VERTICAL,
            GUI_KEY_CONTENT: [
                {
                    GUI_KEY_TYPE: GUI_LABEL,
                    GUI_KEY_TEXT: {
                        'fr': 'GUI_LABEL texte fr',
                        'en': 'GUI_LABEL text en',
                    },
                    GUI_KEY_FONT: {
                        GUI_KEY_SIZE: 10,
                        GUI_KEY_BOLD: True,
                    },
                },
                {
                    GUI_KEY_TYPE: GUI_LABEL,
                    GUI_KEY_TEXT: {
                        'fr': 'GUI_LABEL <font color=\"#DF0101\">texte rouge fr</font>',
                        'en': 'GUI_LABEL <font color=\"#DF0101\">text red en</font>',
                    },
                    GUI_KEY_FONT: {
                        GUI_KEY_BOLD: True,
                    },
                },
                {
                    GUI_KEY_TYPE: GUI_LINE_EDIT,
                    GUI_KEY_TEXT: kernel.get_line_edit,
                    GUI_KEY_FONT: {
                        GUI_KEY_SIZE: 10,
                        GUI_KEY_BOLD: False,
                    },
                    GUI_KEY_ACTION: kernel.set_line_edit
                },
                {
                    GUI_KEY_TYPE: GUI_BUTTON,
                    GUI_KEY_TEXT: kernel.get_text_button,
                    GUI_KEY_ACTION: kernel.clicked_button
                },
                {
                    GUI_KEY_TYPE: GUI_COMBOBOX,
                    GUI_KEY_TITLE: 'GUI_COMBOBOX',
                    GUI_KEY_TOOL_TIP: {
                        'fr': 'GUI_COMBOBOX fr',
                        'en': 'GUI_COMBOBOX en',
                    },
                    GUI_KEY_CHOICES: kernel.get_my_combobox,
                    GUI_KEY_CHECK: kernel.get_my_combobox_index(),
                    GUI_KEY_ACTION: kernel.set_my_combobox_index
                },
                {
                    GUI_KEY_TYPE: GUI_COMBOBOX_RADIO,
                    GUI_KEY_TITLE: {
                        'fr': 'GUI_COMBOBOX_RADIO fr',
                        'en': 'GUI_COMBOBOX_RADIO en',
                    },
                    GUI_KEY_TOOL_TIP: [
                        {
                            'fr': 'GUI_COMBOBOX_RADIO fr Rouge',
                            'en': 'GUI_COMBOBOX_RADIO en Red',
                        }, {
                            'fr': 'GUI_COMBOBOX_RADIO fr Vert',
                            'en': 'GUI_COMBOBOX_RADIO en Red',
                        }, {
                            'fr': 'GUI_COMBOBOX_RADIO fr Bleu',
                            'en': 'GUI_COMBOBOX_RADIO en Blue',
                        },
                    ],
                    GUI_KEY_CHOICES: kernel.get_my_combobox_radio,
                    GUI_KEY_CHECK: kernel.get_my_combobox_radio_index(),
                    GUI_KEY_ACTION: kernel.set_my_combobox_radio_index,
                },
                {
                    GUI_KEY_TYPE: GUI_COMBOBOX_CHECK,
                    GUI_KEY_ORIENTATION: GUI_GRID,
                    GUI_KEY_GRID_COLUMNS: 4,
                    GUI_KEY_TITLE: {
                        'fr': 'GUI_COMBOBOX_CHECK fr',
                        'en': 'GUI_COMBOBOX_CHECK en',
                    },
                    GUI_KEY_TOOL_TIP: {
                        'fr': 'GUI_COMBOBOX_CHECK fr',
                        'en': 'GUI_COMBOBOX_CHECK en',
                    },
                    GUI_KEY_CHOICES: kernel.get_my_combobox_check,
                    GUI_KEY_CHECKES: kernel.get_my_combobox_check_checkes(),
                    GUI_KEY_ACTION: kernel.set_my_combobox_check_index,
                },
                {
                    GUI_KEY_TYPE: GUI_TEXT_EDIT,
                    GUI_KEY_TEXT: kernel.get_text_edit,
                    GUI_KEY_FONT: {
                        GUI_KEY_SIZE: 10,
                        GUI_KEY_BOLD: False,
                    },
                    GUI_KEY_ACTION: kernel.set_text_edit
                },
                {
                    GUI_KEY_TYPE: GUI_BUTTON,
                    GUI_KEY_TEXT: kernel.get_text_button_icon,
                    GUI_KEY_ICON: 'resources/start.png',
                    GUI_KEY_ACTION: kernel.clicked_button_icon
                },
                {
                    GUI_KEY_TYPE: GUI_BOX_LAYOUT,
                    GUI_KEY_ORIENTATION: GUI_HORIZONTAL,
                    GUI_KEY_CONTENT: [
                        {
                            GUI_KEY_TYPE: GUI_IMAGE,
                            GUI_KEY_SOURCE: 'resources/beach.png',
                            GUI_KEY_WIDTH: 300
                        },
                        {
                            GUI_KEY_TYPE: GUI_FIGURE,
                            GUI_KEY_CLASS_FIGURE: ClassFigure
                        },
                    ],
                }
            ],
        },
    ],
}

console = dict()
console.update({'TITLE': 'QBuilder (2.0)', }, )
console.update({'LANGUAGE': kLanguage.get_acronym(), }, )
console.update({'MENU': menu, }, )
console.update({'LAYOUT': layout, }, )

q_builder(console, [kLanguage, kMenu, kernel], lambda: kernel.run())
