# -*- coding: utf-8 -*-
"""

"""
from qbuilder import QBuilder


class KernelLanguage:
    __slots__ = ('acronyms', 'builder', 'denominations', 'index')

    def __init__(self):
        self.acronyms = ['fr', 'en', 'es', ]  # the first is the default language
        self.builder = None
        self.denominations = {
            'fr': ['Français', 'Anglais', 'Espagnol', ],
            'en': ['French', 'English', 'Spanish', ],
            'es': ['Francés', 'Inglés', 'Español', ],
        }
        self.index = 0

    def set(self, builder: QBuilder):
        """
        Mandatory method for q_builder
        Memorization in order to be able to call change_language.
        :param builder:
        :return:
        """
        self.builder = builder
        for acronym in self.acronyms:
            try:
                self.denominations[acronym]
            except KeyError:
                builder.warning('KernelLanguage', 'Not initialize ' + acronym + ' in denominations.')
        for acronym in self.denominations.keys():
            if acronym not in self.acronyms:
                builder.warning('KernelLanguage', 'Not initialize ' + acronym + ' in acronyms.')

    def get_languages(self, acronym):
        return self.denominations[acronym]

    def get_acronym(self):
        return self.acronyms[self.index]

    def get_language_index(self):
        return self.index

    def set_language_index(self, ind, _):
        self.index = ind
        self.builder.action_language(self.acronyms[ind])


kLanguage = KernelLanguage()
