# -*- coding: utf-8 -*-
"""

"""
from qbuilder import q_builder
from builder_const import *

from PyQt5 import QtWidgets

from test_kernel_language import kLanguage


class Kernel:
    def run(self):
        pass


kernel = Kernel()


class KernelMenu:
    __slots__ = 'builder', 'verbose'

    def __init__(self):
        self.verbose = True
        self.builder = None

    def set(self, builder):
        """
        Memorization in order to be able to call change_language.
        :param builder:
        :return:
        """
        self.builder = builder

    def noisy(self, msg: str):
        """
        Method to display trace
        :param msg: message
        """
        if self.verbose:
            print('[KernelMenu] ' + msg)

    def open(self):
        file_name = QtWidgets.QFileDialog.getSaveFileName(self.builder, "Open Address Book", "",
                                                          "Address Book (*.abk);;All Files (*)")
        self.noisy('Open ' + file_name[0])

    def save(self):
        file_name = QtWidgets.QFileDialog.getSaveFileName(self.builder, "Save Address Book", "",
                                                          "Address Book (*.abk);;All Files (*)")
        self.noisy('Save ' + file_name[0])

    def quit(self):
        self.noisy('Quit')
        self.builder.close()

    def help(self):
        self.noisy('Help')

    def about(self):
        self.noisy('About')


kMenu = KernelMenu()

menu = [
    (
        {
            'fr': '&Fichier',
            'en': '&File',
        },
        [
            (
                {
                    'fr': '&Ouvrir',
                    'en': '&Open',
                },
                kMenu.open,
                'CTRL+O',
            ),
            (
                {
                    'fr': '&Sauvegarder',
                    'en': '&Save',
                },
                kMenu.save,
                'CTRL+S',
            ),
            GUI_SEPARATOR,
            (
                {
                    'fr': '&Quitter',
                    'en': '&Quit',
                },
                kMenu.quit,
                'CTRL+Q',
            )
        ],
    ),
    (
        {
            'fr': '&Aide',
            'en': '&Help',
        },
        [
            (
                {
                    'fr': '&Sauvegarder',
                    'en': '&Save',
                },
                kMenu.help,
                {
                    'fr': 'CTRL+A',
                    'en': 'CTRL+H',
                },
            ),
            GUI_SEPARATOR,
            (
                {
                    'fr': '&A propos de',
                    'en': '&About',
                },
                kMenu.about
            ),
        ],
    ),
]

layout = {
    GUI_KEY_TYPE: GUI_BOX_LAYOUT,
    GUI_KEY_ORIENTATION: GUI_VERTICAL,
    GUI_KEY_CONTENT: [
        {
            GUI_KEY_TYPE: GUI_BOX_LAYOUT,
            GUI_KEY_ORIENTATION: GUI_HORIZONTAL,
            GUI_KEY_CONTENT: [
                {
                    GUI_KEY_TYPE: GUI_LABEL,
                    GUI_KEY_TEXT: {
                        'fr': '<font color=\"#DF0101\">Langage de la console</font>',
                        'en': '<font color=\"#DF0101\">Console language</font>',
                    },
                    GUI_KEY_FONT: {'Bold': True, },
                },
                {
                    GUI_KEY_TYPE: GUI_COMBOBOX,
                    GUI_KEY_TOOL_TIP: {
                        'fr': 'Choix de la langue',
                        'en': 'Choice of language',
                    },
                    GUI_KEY_CHOICES: kLanguage.get_languages,
                    GUI_KEY_CHECK: kLanguage.get_language_index(),
                    GUI_KEY_ACTION: kLanguage.set_language_index
                },
            ],
        },
    ],
}

console = dict()
console.update({'TITLE': 'QBuilder (2.0)', }, )
console.update({'LANGUAGE': kLanguage.get_acronym(), }, )
console.update({'MENU': menu, }, )
console.update({'LAYOUT': layout, }, )

q_builder(console, [kLanguage, kMenu], lambda: kernel.run())
